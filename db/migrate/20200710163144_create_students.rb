class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :name
      t.string :student_class
      t.string :phone
      t.text :address

      t.timestamps
    end
  end
end
