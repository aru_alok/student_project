class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  # GET /students
  # GET /students.json
  def index
    @current_user||=  current_user
    if current_user.admin?
      @users= User.all - [current_user]
      render template: 'students/admin'#,:locals => { :users => @users}
    else
      @students = current_user.students
    end
    @student = Student.new
    respond_to do |format|
      format.json {render json: { data: @students}}
      format.html
    end
  end

  # GET /students/1
  # GET /students/1.json
  def show
    @current_user = @student.user
    respond_to do |format|
      format.json {render json: { data: @student}}
      format.html
      format.js
    end
  end

  def get_student_by_user
    @user = User.find(params[:user_id])
    @students = @user.students
    respond_to do |format|
      #format.json {render json: { data: @student}}
      format.html
      format.js
    end
  end
  # POST /students
  # POST /students.json
  def create
    current_user = User.find(params[:student][:user_id])
    @student = current_user.students.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        # format.json { render :show, status: :created, location: @student }
        format.json {render :json => {:status => 'success'}}
        format.js
      else
        format.html { render :new }
        format.json {render :json => {:status => 'false', errors: @student.errors.full_messages}}
        # format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json {render :json => {:status => 'success'}}
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json {render :json => {:status => 'success'}}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def student_params
      params.require(:student).permit(:name, :student_class, :phone, :address)
    end
end
