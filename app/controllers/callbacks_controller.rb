class CallbacksController < Devise::OmniauthCallbacksController

	#google callbacks
  def google_oauth2

    @user = User.from_omniauth(request.env["omniauth.auth"])
    if @user.persisted?
    	sign_in_and_redirect @user,  notice: "User Sign In Successfully"
    else
    	redirect_to new_user_registration_url, alert: @user.errors.full_messages.join("\n")
    end
  end
end

