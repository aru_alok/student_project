class Student < ApplicationRecord

	validates_presence_of :name, :student_class, :phone, :address
	validates :phone, uniqueness: true, numericality: true
	validates_length_of :phone, minimum: 10, maximum: 10
	belongs_to :user
end