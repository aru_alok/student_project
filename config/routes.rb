Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks"}
  resources :students
  root 'students#index'
  get '/get_student_by_user' => 'students#get_student_by_user'
end
